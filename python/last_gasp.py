def sample_sizer(data, tholds):
  nobs = set()
  for ltr, thold in tholds.iteritems():
      last_key = "%slast" % ltr
      true_key = "%strue" % ltr
      conf_key = "%sconf" % ltr
      data = data[data[last_key] != data[true_key]]
      data = data[data[conf_key] > thold]
      [nobs.add(x) for x in data.index.tolist()]
  print "%d obs" % len(nobs)

def adjust_scores(sub_df): 

def n_guess(data, tholds, v1, v2):
  for x, y in itertools.permutations(tholds.iteritems(), 2):
       
      var_name_x = x[0]
      var_name_y = y[0]
      if var_name_x == "%s" % v1 and var_name_y == "%s" % v2:
          store = []
          conf_x = x[1]
          conf_y = y[1]

          last_key_x = "%slast" % x[0]
          last_key_y = "%slast" % y[0]
          true_key_x = "%strue" % x[0]
          true_key_y = "%strue" % y[0]
          conf_key_x = "%sconf" % x[0]
          conf_key_y = "%sconf" % y[0]
          pred_key_x = "%spred" % x[0]
          pred_key_y = "%spred" % y[0]
      
          for new_conf in np.arange(0, conf_y, 0.01):
              right = len(df[(df[conf_key_x] > conf_x) & (df[last_key_x] != df[pred_key_x]) & (df[conf_key_y] > new_conf) & (df[pred_key_x] == df[true_key_x]) & (df[pred_key_y] == df[true_key_y])])
              wrong = len(df[(df[conf_key_x] > conf_x) & (df[last_key_x] != df[pred_key_x]) & (df[conf_key_y] > new_conf) & (df[pred_key_x] == df[true_key_x]) & (df[pred_key_y] != df[true_key_y])])
              gain = right - wrong
              store.append((new_conf, gain))
          return store

  
