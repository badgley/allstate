import argparse
import string
import pandas as pd

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("fname", help="fname of dataset")
    return parser.parse_args()

def evaluate_errors(data, var_name="G"):
    pred_name = "%spred" % var_name
    last_name = "%slast" % var_name
    true_name = "%strue" % var_name

    type_i = pd.np.mean((data[last_name] != data[pred_name]) & 
                        (data[pred_name] != data[true_name]))
    type_ii = pd.np.mean((data[last_name] == data[pred_name]) & 
                        (data[pred_name] != data[true_name]))
    return (var_name, type_i, type_ii)

def main():
    args = arg_parser()
    df = pd.read_csv(args.fname)
    for ltr in string.ascii_uppercase[:7]:
        error = evaluate_errors(df, var_name=ltr)
        print error

if __name__ == "__main__":
    main()

