#!/usr/bin/python

import argparse
import os
import string

import pandas as pd

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("fname", help="fname of dataset")
    return parser.parse_args()

def policy_changes(data, cumulative=False):
    changes = data[['A','B','C','D','E','F','G']].diff()
    changes = changes.fillna(0)
    changes[changes != 0] = 1
    changes['n_change_xo_g'] = changes[[x for x in string.ascii_uppercase[:6]]].sum(axis=1)
    changes['n_change'] = changes.sum(axis=1)
    if cumulative:
        changes = changes.cumsum()
    return changes

def main():
    args = arg_parser()
    df = pd.read_csv(args.fname)
    groups = df.groupby('customer_ID')
    changes = groups.apply(policy_changes)
    cumchanges = groups.apply(policy_changes, cumulative=True)
    changes["customer_ID"] = df['customer_ID']
    changes = changes.join(cumchanges, lsuffix='_norm', rsuffix="_cum")
    
    changes.set_index("customer_ID", inplace=True)
    out_fname = "/%s_policy_changes.csv" % os.path.basename(args.fname)[:-4]
    changes.to_csv("/tmp/%s" % out_fname)
    print "\n"
    print "wrote new data to /tmp%s" % out_fname
    print "\n"
    

if __name__ == "__main__":
    main()
