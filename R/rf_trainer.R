nNodes <- 8
source('/Users/aaronpolhamus/Dropbox/Polhooper/PROJECTS/AllState/usersettings.r')
sapply(c('ggplot2', 'plyr', 'chron', 'grid','scales','msm', 'parallel', 'randomForest', 'class', 'reshape2',
         'foreach', 'doMC', 'vwr', 'data.table'), function(x) library(x, character.only = TRUE))

###############
#..DATA PREP..#
###############

train <- 'train.csv'
test <- 'test_v2.csv'

formatData <- function(fname, useCache=FALSE, rfFill = TRUE){
  cacheName <- paste(substr(fname,1, nchar(fname)-4), '.Rdata',sep="")
  if(useCache){
    load(cacheName)
    return
  }
  inData <- read.csv(fname, as.is=TRUE)
    
  #..define missing entries for car_value as NA: 
  inData$car_value[inData$car_value == ''] <- NA
  
  factorFunc <- function(inData2 = inData){
    
    #..express car_value as a factor
    inData2$car_value <- factor(inData2$car_value)
    
    #..express risk_factor as factor: 
    inData2$risk_factor <- factor(inData2$risk_factor)
    
    #..express C_previous as factor: 
    inData2$C_previous <- factor(inData2$C_previous)
    
    #..convert features into factors: 
    inData2[LETTERS[1:7]] <- lapply(inData2[LETTERS[1:7]], as.factor)  
  
    inData2
  }
  
    
  #..default to gap fill with random forest. recode NAs as new factor levels otherwise
  if(rfFill){
    
    inData <- factorFunc()
    
    #..rfImpute fill wrt to a non-NA containing response variable. For now will wrt to C, where the RF
    #..makes the greatest contribution to accuracy wrt to last-observed plan
    trainFrame <- inData
    if(any(inData$record_type == 1)) trainFrame <- trainFrame[trainFrame$record_type != 1, ]
    #..at the moment we go through this step prior to the re-casting of the data may want to change this if the rfImpute strategy looks promising
    #..this if the rfImpute strategy looks promising
    trainFrame <- trainFrame[tapply(1:nrow(trainFrame), trainFrame$customer_ID, max), ]
    Xlist <- c('location', 'group_size', 'homeowner', 'car_age', 'car_value', 'risk_factor', 
               'age_oldest', 'age_youngest', 'married_couple', 'C_previous', 'duration_previous', LETTERS[1:7][-3], 'cost')
    X <- trainFrame[, Xlist]
    Y <- trainFrame$C
    #..subsegment the data, rfImpute, and recombine: 
    
    slices <- 16 
    idx <- rep(1:slices, each = ceiling(nrow(trainFrame)/slices))
    idx <- idx[1:nrow(trainFrame)]
    
    cl <- makeCluster(nNodes)
    clusterExport(cl, c('idx', 'slices', 'X', 'Y'))
      imputedData <- do.call('rbind', parLapply(cl, 1:slices, function(SLICE){
        require(randomForest)
        rfImpute(X[idx == SLICE, ], Y[idx == SLICE])
      }))
    stopCluster(cl)
    imputedData$customer_ID <- trainFrame$customer_ID
    
    #..swap out old for interpolated data: 
    filled <- names(inData)[apply(inData, 2, function(x) any(is.na(x)))]
    inData <- inData[, !names(inData) %in% filled]
    inData <- join(inData, imputedData[, c('customer_ID', filled)])
    
  } else {
    #..in the test set data, some locations are NAs. This is a roughfix: 
    if(any(is.na(inData$location))) inData$location[is.na(inData$location)] <- mean(inData$location, na.rm = TRUE)
    
    #..express duration_previous as a numeric()
    inData <- inData[order(inData$customer_ID, inData$shopping_pt), ]
    inData$duration_previous <- as.numeric(unlist(tapply(inData$duration_previous, inData$customer_ID, function(VEC){ 
      if(all(!is.na(VEC))){ 
        return(VEC)
      } else { 
        if(all(is.na(VEC))){ 
          return(rep(0, length(VEC)))
        } else { 
          return(rep(mean(VEC, na.rm = TRUE), length(VEC)))
        }
      }
    })))
    
    #..recode NA factor levels to 'na', such that they are not specially-handled
     inData[is.na(inData)] <- 'na'
    
  }
  
  inData <- factorFunc()
  
  #..create a new "outcome" column that is a concatenation of all policy options: 
  inData$outcome <- apply(inData[, LETTERS[1:7]], 1, function(x) paste(x, collapse = ''))
  
  #..setup time to be timestamped data: 
  inData$time <- chron::times(paste0(inData$time, ':00'))
  
  #..express state as column of binaries
  inData <- cbind(inData, model.matrix(~factor(state), inData))

  save(inData, file = cacheName)
  gc()
  return(inData)
}

train <- formatData(train)
#train <- formatData(train, rfFill = FALSE)
test <- formatData(test)
#test <- formatData(test, rfFill = FALSE)

#..List of initial variables that we want to use as covariates. we will add more later. : 
VARLIST <- c("group_size", "car_age", 
             "age_oldest", "age_youngest", "duration_previous", 
             "location", "cost", "married_couple", "homeowner", 
             "C_previous", "car_value", "risk_factor")

#..truncate the training data to simulate the kaggle data set
truncateData <- function(useCache=TRUE, inData, compData = test) {
  cacheName <- "trainTable.RData"
  if(useCache){
    load(cacheName)
    return
  }
  train1 <- inData[inData$record_type != 1, ]
  tmp <- table(train1$customer_ID)
  idCounts <- data.frame(ID = as.numeric(names(tmp)), counts = c(tmp), stringsAsFactors = FALSE)
  rownames(idCounts) <- NULL
  
  Ns <- table(table(compData$customer_ID))
  Ns <- c(floor(nrow(idCounts)*(Ns/sum(Ns))))
  
  sampleFrame <- idCounts
  truncList <- list()
  for(GROUP in 2:11){
    print(GROUP)
    N <- Ns[GROUP - 1]
  
    sampleFrame <- sampleFrame[sampleFrame$counts >= GROUP, ]
    
    current <- length(which(sampleFrame$counts == GROUP))
    
    if(current >= N){ 
      idsToStore <- sample(sampleFrame[sampleFrame$counts == GROUP, 'ID'], N)
    } else if(current > 0 & current < N) { 
      idsToStore <- sampleFrame[sampleFrame$counts == GROUP, 'ID']
      sampleFrame <- sampleFrame[!sampleFrame$counts == GROUP, ]
      N <- N - length(idsToStore)
      idx <- sample(1:nrow(sampleFrame), N)
      idsToStore <- c(idsToStore, sampleFrame[idx, 'ID'])
      sampleFrame <- sampleFrame[-idx, ]
    } else { 
      idx <- sample(1:nrow(sampleFrame), N)
      idsToStore <- sampleFrame[idx, 'ID']
      sampleFrame <- sampleFrame[-idx, ]
    }
    truncList[[GROUP - 1]] <- idsToStore
  }
  
  print(environment)
  
  cl <- makeCluster(nNodes)
  clusterExport(cl, c('truncList', 'train1'), envir = environment())
  IDX <- unlist(parSapply(cl, 1:length(truncList), function(i){ 
    trunc <- i + 1
    c(sapply(truncList[[i]], function(ID){ 
      vec <- which(train1$customer_ID == ID)
      vec <- vec[1:trunc]
      vec
    }))
  }))
  stopCluster(cl)
  trainTable <- train1[IDX, ]
  return(trainTable)
} 

trainmod <- truncateData(inData = train, compData = test)

makeTestMod <- function(inData, changesFn = 'testmod_policy_changes.csv') {

  dt <- data.table(inData)
  setkey(dt, 'customer_ID')
  timeVars <- dt[, list(totTime = as.numeric(max(time) - min(time)), 
                        perInst = as.numeric(max(time) - min(time))/max(shopping_pt)), by = key(dt)]
  timeVars <- as.data.frame(timeVars)
  out <- join(inData, timeVars)
  changes <- read.csv(changesFn, as.is = TRUE)[, -1]
  changes <- data.table(changes)
  setkey(changes, 'customer_ID')
  muteData <- as.data.frame(changes[, list(
    Adelt0 = sum(A), Bdelt0 = sum(B), Cdelt0 = sum(C), Ddelt0 = sum(D), Edelt0 = sum(E), Fdelt0 = sum(F), Gdelt0 = sum(G), 
    Adelt1 = sum(A)/length(A), Bdelt1 = sum(B)/length(B), Cdelt1 = sum(C)/length(C), Ddelt1 = sum(D)/length(D), Edelt1 = sum(E)/length(E), 
      Fdelt1 = sum(F)/length(F), Gdelt1 = sum(G)/length(G)), by = key(changes)])
  out <- join(out, muteData)
  return(out)
}
testmod <- makeTestMod(test)
save(testmod, file = 'testmod.RData')
trainmod <- makeTestMod(inData = trainmod, changesFn = 'trainmod_policy_changes.csv')
save(trainmod, file = 'trainmod.RData') 

#..set the variables that we'll use for the rf analysis
rfList <- c(VARLIST, LETTERS[1:7], 'shopping_pt', 'totTime', 'perInst', 
            names(trainmod)[grep('factor[(]state[])]', names(trainmod))], names(trainmod)[grep('norm|cum', names(trainmod))])


# from http://stackoverflow.com/questions/7494848 
`%notin%` <- function(x,y) !(x %in% y) 

myExperiment <- function(data) {
    featureSet <- c(VARLIST, LETTERS[1:7], 'shopping_pt', 'totTime', 'perInst', 
            names(train)[grep('factor[(]state[])]', names(trainmod))], paste0(LETTERS[1:7], 'delt0'), paste0(LETTERS[1:7], 'delt1'))

#..extract last-observed data: 
lastIdx <- tapply(1:nrow(trainmod), trainmod$customer_ID, max) #..NOTE: THIS RE-ORDERS THE OBSERVATIONS
rfX <- trainmod[lastIdx, rfList]

#################
#..RF TRAINING..#
#################

#..iteration (1): run the raw forest

cl <- makeCluster(nNode - 1)
clusterExport(cl, c('train', 'rfX', 'trainmod', 'rfList'))
rfData <- parLapply(cl, LETTERS[1:7], function(FEATURE){
    
  require(randomForest)
  
  rfY <- train[train$record_type == 1 & train$customer_ID %in% trainmod$customer_ID, FEATURE]
  
  Xtrain <- rfX[, rfList] #..take out last observed feature value. allow this one to wander around a bit more
  
  rfFit <- randomForest(Xtrain, rfY, mtry = ceiling(ncol(Xtrain)/3))
  
  evalFrame <- data.frame(last = Xtrain[, FEATURE], predicted = as.character(predict(rfFit)), actual = rfY, 
                          certainty = apply(predict(rfFit, type = 'prob'), 1, max))
  
  
  return(list(rfFit = rfFit, evalFrame = evalFrame))
})
stopCluster(cl)
names(rfData) <- LETTERS[1:7]
save(rfData, file = 'rf_w_new_changes.RData')

#..add predicted level and certainty to last 
predictedLevels <- as.data.frame(lapply(rfDataTop, function(RF) predict(RF$rfFit)))
names(predictedLevels) <- paste0(names(predictedLevels), 'pred')
confidenceLevels <- as.data.frame(lapply(rfDataTop, function(RF) apply(predict(RF$rfFit, type = 'prob'), 1, max)))
names(confidenceLevels) <- paste0(names(confidenceLevels), 'conf')

rfX <- cbind(rfX, predictedLevels, confidenceLevels)

#########################################################################################
#..THIS TRAINING SCRIPTS ALLOWS DIFFERENT VARIABLES TO BE PASSED TO DIFFERENT FEATURES..#
#########################################################################################

params <- lapply(LETTERS[1:7], function(FEATURE) names(rfData2[[FEATURE]]$rfFit$importance[order(rfData2[[FEATURE]]$rfFit$importance, decreasing = TRUE), ])[1:27])
names(params) <- LETTERS[1:7]

cl <- makeCluster(7)
clusterExport(cl, c('train', 'rfX', 'trainmod', 'lastIdx', 'params'))
rfData3 <- parLapply(cl, LETTERS[1:7], function(FEATURE){
  require(randomForest)
  
  Xtrain <- rfX[, params[[FEATURE]]]
  
  rfY <- train[train$record_type == 1 & train$customer_ID %in% trainmod$customer_ID, FEATURE]
  
  rfFit <- randomForest(Xtrain, rfY)
  
  evalFrame <- data.frame(last = Xtrain[, FEATURE], predicted = predict(rfFit), actual = rfY, 
                          certainty = apply(predict(rfFit, type = 'prob'), 1, max), id = train[train$record_type == 1 & train$customer_ID %in% trainmod$customer_ID, 'customer_ID'])
  
  return(list(rfFit = rfFit, evalFrame = evalFrame))
})
stopCluster(cl)
names(rfData3) <- LETTERS[1:7]
save(rfData3, file = 'rfData3.RData')

